package co.com.masivian;

public class Printer implements PrinterIface {

    private int PAGENUMBER;
    private int PAGEOFFSET;
    private int ROWOFFSET;

    @Override
    public int[] calculateP() {
        int J = 1;
        int K = 1;
        int P[] = new int[M + 1];
        boolean JPRIME;
        int ORD = 2;
        int SQUARE = 9;
        int N;
        int MULT[] = new int[ORDMAX + 1];
        while (K < M) {
            do {
                J += 2;
                if (J == SQUARE) {
                    ORD++;
                    SQUARE = P[ORD] * P[ORD];
                    MULT[ORD - 1] = J;
                }
                N = 2;
                JPRIME = true;
                while (N < ORD && JPRIME) {
                    while (MULT[N] < J)
                        MULT[N] += P[N] + P[N];
                    if (MULT[N] == J) JPRIME = false;
                    N++;
                }
            } while (!JPRIME);
            K++;
            P[K] = J;
        }
        return P;
    }

    @Override
    public void printerPage(int P[]) {
        int C;
        PAGENUMBER = 1;
        PAGEOFFSET = 1;
        while (PAGEOFFSET <= M) {
            printerOut();
            for (ROWOFFSET = PAGEOFFSET; ROWOFFSET <= PAGEOFFSET + RR - 1; ROWOFFSET++) {
                for (C = 0; C <= CC - 1; C++) {
                    if (ROWOFFSET + C * RR <= M) {
                        System.out.printf("%10d", P[ROWOFFSET + C * RR]);
                    }
                }
                System.out.println();
            }
            System.out.println("\f");
            PAGENUMBER++;
            PAGEOFFSET += RR * CC;
        }
    }

    @Override
    public void printerOut() {
        System.out.print("The First ");
        System.out.print(Integer.toString(M));
        System.out.print(" Prime Numbers === Page ");
        System.out.print(Integer.toString(PAGENUMBER));
        System.out.println("\n");
    }

    public static void main(String[] args) {
        Printer p = new Printer();
        int[] P = p.calculateP();
        p.printerPage(P);
    }


}

