package co.com.masivian;

public interface PrinterIface {

    final int M = 1000;
    final int RR = 50;
    final int CC = 4;
    final int ORDMAX = 30;

    int [] calculateP();

    void printerPage(int P[]);

    void printerOut();



}
